#pragma once

#include "Node.h"
#include <iostream>

using namespace std;

class Stack
{
	Node<int>* front;       // Pointer to front queue node
	Node<int>* rear;        // Pointer to rear queue node
	int size;                // Number of elements in queue

public:
	Stack()
	{
		front = rear = new Node<int>(); // Creates a new node, sets bottom and top pointers to point at it
		size = 0;
	}
	~Stack()
	{

	}

	void push(const int& it) // Put element on front of line
	{
		if (front->next == nullptr) // No elements in the deck
		{
			front->next = new Node<int>(it, nullptr); //Creates a new link, sets the front's 'next' pointer to it
			rear = front->next; //Sets rear to point to the new link
		}
		else
		{
			Node<int>* temp = front->next; //creates a temporary pointer that indicates what is going to be 2nd from front
			front->next = new Node<int>(it, nullptr); //Creates a new link, sets the front's next pointer to it
			front->next->next = temp; // Sets the 'next' pointer in the new node to the old front node
		}

		size++;
	}

	int pop() // Remove element from back of line
	{
		//assert(size != 0, "Queue is empty"); 
		if (size == 0)
		{
			cout << "Stack is empty. Invalid pop." << endl;
			abort();
		}

		else
		{
			int it = front->next->element;  // Store popped value
			Node<int>* ltemp = front->next; // Hold popped link

			if (front->next == rear) //Popping the last element
			{
				front->next = nullptr;
				rear = front;
			}
			else
			{
				front->next = front->next->next;       // Advance front
			}
			
			delete ltemp;                    // Delete link
			size--;
			return it;                       // Return element value
		}
	}

	void printAll() //Prints and empties the stack
	{
		cout << "Printing Stack: " << endl;
		while (size > 0)
		{
			if (size == 1) //Last card
			{
				cout << pop() << ". " << endl << endl;
			}
			else
			{
				cout << pop() << ", ";
			}
		}
	}

	virtual int length() const
	{
		return size;
	}
};

