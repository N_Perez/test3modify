#pragma once
template < typename E> class Node
{

public:

	E element;      // Value for this node
	Node *next;        // Pointer to next node in list

	Node(const E& elemval, Node* nextval = nullptr) //Constructor, with an initial value supplied
	{
		element = elemval;
		next = nextval;
	}
	Node(Node* nextval = nullptr) //Constructor, without an initial value
	{
		next = nextval;
	}

	~Node()
	{

	}

};


