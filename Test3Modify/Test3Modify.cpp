/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Test 3 Modify
*/



#include <iostream>
#include "Stack.h"
#include "Queue.h"
#include "ldeque.h"

using namespace std;

void fill(Stack& pile, Queue& line, LDeck<int>& deck, int input[], const int& NUMBERS);

int main()
{
	const int NUMBERS = 10;
	Stack pile;
	Queue line;
	LDeck<int> deck;
	int input [NUMBERS] = { 1, 2, 3, 7, 8, 9, 4, 5, 6, 10};

	cout << "Filling a stack, a queue, and a deque of ints..." << endl;
	cout << "Inserting: 1, 2, 3, 7, 8, 9, 4, 5, 6, 10" << endl;
	fill(pile, line, deck, input, NUMBERS);
	cout << "Done filling." << endl << endl;
	pile.printAll();
	line.printAll();
	cout << "Printing Deque:" << endl;
	deck.readOut();


	system("pause");
	return 0;
}

void fill(Stack& pile, Queue& line, LDeck<int>& deck,int input[], const int& NUMBERS)
{
	for (int x = 0; x < NUMBERS; x++)
	{
		pile.push(input[x]);
		line.enqueue(input[x]);
		deck.enqueue(input[x]);
	}
	deck.sortBubble();
};