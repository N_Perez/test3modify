/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Game Project
Deque
*/
#pragma once

#ifndef LDEQUE_H
#define LDEQUE_H

#include "link.h"
#include <fstream>
#include <string> 
#include <iomanip>
#include <stdio.h>

using namespace std;

template <typename E> class LDeck {
private:
	Link<E>* front;       // Pointer to front queue node
	Link<E>* rear;        // Pointer to rear queue node
	int size;                // Number of elements in queue

protected:


	void insertBefore(Link<E>* linkOne, Link<E>* linkTwo) //Inserts the first link before the second link
	{
		Link<E>* linkOnePrev = linkOne->prev;
		Link<E>* linkOneNext = linkOne->next;
		//stores a copy of what link one's previous and next pointers are pointing two
		Link<E>* linkTwoPrev = linkTwo->prev;
		Link<E>* linkTwoNext = linkTwo->next;
		//stores a copy of what link two's previous and next pointers are pointing two

		linkOnePrev->next = linkOneNext; //Sets the next pointer in the link before link one's old position to what was after link one
		if (linkOneNext != NULL) linkOneNext->prev = linkOnePrev; //Sets the previous pointer in the link after link one's old position to what was before link one if there is a link after link one

		linkOne->next = linkTwo; //Sets link one's next pointer to point to link two
		linkOne->prev = linkTwo->prev; //Sets Link one's prev pointer to point to what was before link two
		linkTwo->prev = linkOne; //Set link two's prev pointer to point to link One
		linkOne->prev->next = linkOne; //Set the next pointer in the link before link one's new position to point to it

		if (linkOne == rear) //If linkOne was the last card in the deck
		{
			rear = linkOnePrev; //Sets the rear pointer to point to what was before link one's old position.
		}

	}

	void insertAfter(Link<E>* linkOne, Link<E>* linkTwo) //Inserts the first link after the second link
	{
		Link<E>* linkOnePrev = linkOne->prev;
		Link<E>* linkOneNext = linkOne->next;
		//stores a copy of what link one's previous and next pointers are pointing two
		Link<E>* linkTwoPrev = linkTwo->prev;
		Link<E>* linkTwoNext = linkTwo->next;
		//stores a copy of what link two's previous and next pointers are pointing two

		linkOnePrev->next = linkOneNext; //Sets the next pointer in the link before link one's old position to what was after link one
		if (linkOneNext != NULL) linkOneNext->prev = linkOnePrev; //Sets the previous pointer in the link after link one's old position to what was before link one if there is a link after link one

		linkOne->next = linkTwo->next; //Sets link one's next pointer to point to what was after link Two
		linkTwo->next = linkOne; // Sets link two's next pointer to point to link one
		linkOne->prev = linkTwo; // sets linkOne's prev pointer to point to link Two, which is now ahead of it
		if (linkOne->next != NULL)linkOne->next->prev = linkOne; //Sets the previous pointer in the link after link one's new position to point to it, if a link exists after linkOne's new position


		if (linkOne == rear) //If linkOne was the last card in the deck
		{
			rear = linkOnePrev; //Sets the rear pointer to point to what was before link one's old position.
		}
	}

private:
	void swapLinkVal(Link<E>* linkOne, Link<E>* linkTwo) //Swaps the values of two links
	{
		E linkOneValue = linkOne->element; // Holds the element for link one
		E linkTwoValue = linkTwo->element; // Holds the element for link two

		linkOne->element = linkTwoValue; //Swaps the elements in link one and two
		linkTwo->element = linkOneValue;
	}

	void swapLink(Link<E>*& linkOne, Link<E>*& linkTwo) //Swaps the positions of two links 
	{
		if (linkOne->next == linkTwo) //Right next to each other
		{
			linkOne->next = linkTwo->next; //Link One's next now points to what's after Link Two
			linkTwo->prev = linkOne->prev; //Link Two's prev now points to what's before Link One

			if (linkOne->next != NULL)
			{
				linkOne->next->prev = linkOne;
			}
			if (linkTwo->prev != NULL)
			{
				linkTwo->prev->next = linkTwo;
			}

			linkTwo->next = linkOne;
			linkOne->prev = linkTwo;

			if (linkOne == rear) //If link one was the rear link
			{
				rear = linkTwo; //Link two's now the rear link, since they swapped
			}
			else if (linkTwo == rear) //Otherwise, if linkTwo was the rear link
			{
				rear = linkOne; //link one's now the rear link, since they swapped positions.
			}
		}

		else
		{
			Link<E>* linkOnePrev = linkOne->prev;
			Link<E>* linkOneNext = linkOne->next;
			//stores a copy of what link one's previous and next pointers are pointing two
			Link<E>* linkTwoPrev = linkTwo->prev;
			Link<E>* linkTwoNext = linkTwo->next;
			//stores a copy of what link two's previous and next pointers are pointing two

			linkOne->prev = linkTwoPrev;
			linkOne->next = linkTwoNext;
			//Sets link one's pointers to point to what link two's pointers are pointing to

			linkTwo->prev = linkOnePrev;
			linkTwo->next = linkOneNext;
			//Sets link two's pointers to point to what link one's pointers were pointing to

			if (linkOne->next != NULL)
			{
				linkOne->next->prev = linkOne;
			}
			if (linkOne->prev != NULL)
			{
				linkOne->prev->next = linkOne;
			}
			//Sets the pointers of the links around link one's new position to point to it

			if (linkTwo->prev != NULL)
			{
				linkTwo->prev->next = linkTwo;
			}
			if (linkTwo->prev != NULL)
			{
				linkTwo->prev->next = linkTwo;
			}
			//Sets the pointers of the links around link two's new position to point to it
		}

		Link<E>* temp1 = linkOne;
		Link<E>* temp2 = linkTwo;

		linkOne = temp2;
		linkTwo = temp1;
		return;
	}

	LDeck<E> helpMergeSort(LDeck<E> inDeck, int& swaps, int& compares) // Helper for merge sorting, divides deck in half, recursively, then goes to merge
	{
		if (inDeck.length() == 1 || inDeck.length() == 0) //Only one or no cards in the deck, time to start merging
		{
			return inDeck;
		}
		else //More than one card in the deck, split it in half, run both halves through this again
		{
			int half = 0;
			LDeck<E>* point = &inDeck;
			LDeck<E> firstHalf;
			half = halver(point); //Finds the half-way point for the deck

			for (int x = 1; x <= half; x++) //Pulls the first half of the incoming deck into its own deck
			{
				E hold;
				hold = inDeck.dequeue();
				firstHalf.enqueue(hold);
			}

			//Recurs for the first and second halves
			firstHalf = helpMergeSort(firstHalf, swaps, compares);
			inDeck = helpMergeSort(inDeck, swaps, compares);

			return helpMerge(firstHalf, inDeck, swaps, compares);
		}
	}

	LDeck<E> helpMerge(LDeck<E> first, LDeck<E> second, int& swaps, int& compares) // Helper for merge sorting, combines two decks
	{
		if (first.length() == 0)//First deck's empty
		{
			return second;
		}
		if (second.length() == 0)//Second deck's empty
		{
			return first;
		}

		E hold;
		LDeck<E> tempDeck;

		//Pick the smaller value

		compares++; //about to compare

		if (first.frontValue() < second.frontValue())
		{
			//First value's lower, pull the card out
			hold = first.dequeue();

		}
		else
		{
			//Second is lower, pull the card from the second
			hold = second.dequeue();
		}

		tempDeck = helpMerge(first, second, swaps, compares);  //Deep wizardry,
		tempDeck.endeck(hold);								 // why does this part work

		return tempDeck;
	}



public:
	LDeck() // Constructor 
	{
		front = rear = new Link<E>(); // Creates a new node, sets front and rear pointers to point at it
		size = 0;
	}

	void enqueue(const E& it) // Put element on rear
	{
		rear->next = new Link<E>(it, NULL, NULL); //Sets the rear 'next' pointer to point to a new link
		Link<E>* temp = rear; //creates a temporary pointer that indicates what is now 2nd from rear
		rear = rear->next; //sets the 'rear' pointer to point to the new link
		rear->prev = temp; // sets the new link's previous pointer to the one before it
		size++;
	}

	E dequeue() // Remove element from front
	{
		//assert(size != 0, "Queue is empty"); //how to into asserts
		if (size == 0)
		{
			cout << "Deque is empty. Invalid dequeue" << endl;
			abort();
		}

		else
		{
			E it = front->next->element;  // Store dequeued value
			Link<E>* ltemp = front->next; // Hold dequeued link
			front->next = ltemp->next;       // Advance front
			if (rear == ltemp)
			{
				rear = front; // Dequeue last element
				front->next = NULL;
			}
			else
			{
				front->next->prev = front; //Set new front element's prev pointer to the front ghost link
			}
			delete ltemp;                    // Delete link
			size--;
			return it;                       // Return element value
		}
	}

	//deque functions, but I'll be using "deck" throughout 

	void endeck(const E& it) // Put element on front
	{
		if (front->next == NULL) // No elements in the deck
		{
			front->next = new Link<E>(it, NULL, NULL); //Creates a new link, sets the front's 'next' pointer to it
			front->next->prev = front; //Sets the new link's 'prev' pointer to point to the front link
			rear = front->next; //Sets rear to point to the new link
		}
		else
		{
			front->next->prev = new Link<E>(it, NULL, NULL); //Sets the first element's 'previous' pointer to point to a new link
			Link<E>* temp = front->next; //creates a temporary pointer that indicates what is going to be 2nd from front
			front->next = temp->prev; // Sets the front's 'next' pointer to point to the new link
			front->next->next = temp; // Sets the new link's 'next' pointer to point to what is now the second element from front
			front->next->prev = front; //Sets the new link's 'prev' pointer to point to the front link
		}

		size++;
	}

	E dedeck() // Remove element from back
	{
		//assert(size != 0, "Queue is empty"); 
		if (size == 0)
		{
			cout << "Deque is empty. Invalid dedeck." << endl;
			abort();
		}

		else
		{
			E it = rear->element;  // Store dedecked value
			Link<E>* ltemp = rear; // Hold dedecked link
			rear = ltemp->prev;       // Advance rear

			if (front->next == ltemp)
			{
				front->next = NULL; // Dedecked last element, empty the front's next pointer 
				rear = front;
			}

			delete ltemp;                    // Delete link
			size--;
			return it;                       // Return element value
		}
	}


	void sortBubble()
	{
		if (size == 0)
		{
			cout << "Deque is empty. Invalid sort." << endl;
			abort();
		}
		else
		{
			int swaps = 0, compares = 0;
			bool unsorted = true;
			cout << "Bubble Sorting Deque..." << endl;
			while (unsorted == true)
			{
				Link<E>* tempLeft = front->next; //Points to the first link in the deck
				Link<E>* tempRight = tempLeft->next; //points to the second link in the deck			
													 // These two pointers create a pair to perform bubble sort with

				bool change = false;

				while (tempLeft->next != NULL) //While your front number hasn't hit the end of the deck
				{
					if (tempLeft->element > tempRight->element) //Is the element of the first node greater than the element of the second node?
					{
						//Yes, swap their values, say we made a change. Our front link now has the back link's value, and vice versa.
						//swapLinkVal(tempLeft, tempRight);
						swapLink(tempLeft, tempRight);
						change = true; // Indicates we did something this passthrough
						tempLeft = tempLeft->next;
						tempRight = tempRight->next;
						swaps++;
					}
					else
					{  //No. Our back link becomes our front, the one after that becomes back.
						tempLeft = tempLeft->next;
						tempRight = tempRight->next;
					}
					compares++;
				}
				if (change == false)
				{
					unsorted = false; //Finished sorting

				}
			}
			cout << "Finished bubble sorting." << endl;
			//cout << "Compares: " << compares << " Swaps: " << swaps << endl;
		}
	}

	void sortInsertion()
	{
		if (size == 0)
		{
			cout << "Deque is empty. Invalid sort." << endl;
			abort();
		}
		else
		{
			int swaps = 0, compares = 0;
			Link<E>* lastSorted = front->next; //points to the end of the sorted section, here the first link in the deque
			cout << "Insertion Sorting..." << endl;

			while (rear != lastSorted) //While the end of the sorted section isn't the end of the deck
			{
				Link<E>* tempLook = lastSorted; //Points to the last sorted link
				Link<E>* workingLink = lastSorted->next; //points to link we're working with, here the second link in the deck
				bool swapping = true;
				while (swapping == true)
				{
					if (workingLink->element > tempLook->element) //Is the element in the card we're working on greater than the one we're currently looking at?
					{
						//Yes, insert the link we're working with after the link we're looking at, increment swaps counter
						if (tempLook->next == workingLink) //WorkingLink is immediately after the link we're looking at
						{
							//Don't need to move the link we're working with
							//just need to increment the last sorted marker so that it is pointing to the link we're working with, then work on the next link
							lastSorted = lastSorted->next;//increments sort marker
							swapping = false;//Done working with this link, move to the next
						}
						else
						{
							insertAfter(workingLink, tempLook);
							//lastSorted = lastSorted->next; //increment sort marker //Automatically done by inserting directly rather than using swaps, video advice was array-based
							swaps++;
							swapping = false; //Done swapping for this link, start moving the next one
						}
					}
					else
					{
						//No, move where we're looking one to the left, unless it's about to hit the front ghost link, in which case we place it immediately after 
						if (tempLook->prev == getFront())
						{
							insertAfter(workingLink, tempLook->prev);
							swaps++;
							swapping = false; //Done swapping for this link, start moving the next one
						}
						tempLook = tempLook->prev;
					}
					compares++; //Increments compares
				}
			}
			cout << "Finished insertion sorting. Compares: " << compares << " Swaps: " << swaps << endl;
		}
	}

	void sortSelection()
	{
		if (size == 0)
		{
			cout << "Deque is empty. Invalid sort." << endl;
			abort();
		}
		else
		{
			int swaps = 0, compares = 0;
			Link<E>* lastSorted = front; //points to the end of the sorted section, here the front ghost link, as the rest of the deck is unsorted
			bool unsorted = true;
			cout << "Selection Sorting..." << endl;
			while (lastSorted != rear) //If the rear element is part of the sorted section, we've hit the end and finished.
			{
				Link<E>* selector = lastSorted->next; //Will point to the card with the smallest element in the unsorted section. 
				if (selector->next == NULL) //If there's only one element left in the unsorted section of the list, then it's last in the sorted list, and we just move the sorted marker up
				{
					lastSorted = lastSorted->next;
				}
				else
				{
					bool looking = true;//We're going throught he unsorted section of the list for a number to insert
					Link<E>* tempLook = selector->next; //will be used to help find the smallest element in the unsorted section of the deck. 

					while (looking == true)
					{
						if (tempLook->element < selector->element) //Is the element in the card we're looking at less than the one we've already selected?
						{
							//Yes, the link we're looking at is the new selected link, 
							selector = tempLook;
						}
						//No
						else if (tempLook->next == NULL) //Is tempLook looking at the last link in the deck?
						{
							//Yes, our selector is the minimum of the unsorted section, add it to the end of the sorted section, increment the sorted section marker, start looking for another number
							if (lastSorted->next == selector)
							{
								//The thing we've selected to insert is actually immediately after the last sorted marker, so we don't need to move it, just the last sorted marker
								lastSorted = lastSorted->next;
							}
							else
							{
								insertAfter(selector, lastSorted);
								lastSorted = lastSorted->next;
								swaps++;
							}
							//compares--; //Not really a compare, decrement counter in prep for incoming increment
							looking = false;
						}
						else
						{
							//No, move where we're looking one to the right
							tempLook = tempLook->next;
						}
						compares++; //Increments compares		
					}
				}
			}
			cout << "Finished selection sorting. Compares: " << compares << " Swaps: " << swaps << endl;
		}
	}


	void sortMerge()
	{
		if (size == 0)
		{
			cout << "Deque is empty. Invalid sort." << endl;
			abort();
		}
		else
		{
			int swaps = 0, compares = 0;
			cout << "Merge Sorting..." << endl;

			LDeck<E> tempDeck;

			while (size>0) //Empties out this deck into a temporary subdeck
			{
				E hold;
				hold = dequeue();
				tempDeck.enqueue(hold);
			}

			tempDeck = helpMergeSort(tempDeck, swaps, compares); //Sends the temporary deck into the helper function where it gets sorted. 

																 //cout << "Finished merge sorting. Compares: " << compares << " Swaps: " << swaps << endl;
			cout << "Finished merge sorting. Compares: " << compares << endl; //TEMP - swap count not currently implemented, don't print it

			while (tempDeck.length()>0) //Empties out the sorted subdeck back into the main deck
			{
				E hold;
				hold = tempDeck.dequeue();
				enqueue(hold);
			}
		}

	}


	void fileIn(string filename) //Reads a list of words from a given filename, loads the deck with it
	{
		string line;
		int x;
		ifstream myfile(filename); //Open the word list
		if (myfile.is_open())
		{
			cout << "Loading numbers from \"" << filename << "\"." << endl;
			cout << "Inserting: " << endl;
			while (getline(myfile, line)) // Grabs each line from the text file, stores them in the deck
			{
				cout << line << ", "; //TEMP -- prints when each number is pulled from text file
				x = stoi(line);//Turns the string 'line' into an integer
				enqueue(x); //Stores each incoming number in the deck
			}
			cout << "\b\b" << ". " << endl; //Moves cursor back two characters, replacing the final ", " with ". "
			myfile.close(); // Closes the file
		}

		else
		{
			cout << "Unable to open file.";
			abort(); //Can't open the file, print an error and kill program
		}
	}


	//testing/utility functions

	const E& frontValue() const
	{ // Get front element	  
	  //assert(size != 0, "Queue is empty");
		if (size == 0)
		{
			cout << "Deque is empty. Invalid frontValue." << endl;
			abort();
		}
		else
		{
			return front->next->element;
		}
	}
	const E& rearValue() const
	{ // Get rear element	  
	  //assert(size != 0, "Queue is empty");
		if (size == 0)
		{
			cout << "Deque is empty. Invalid rearValue" << endl;
			abort();
		}
		else
		{
			return rear->element;
		}
	}

	Link<E>* frontLink() const
	{ // Get pointer to the first link with an element in it
	  //assert(size != 0, "Queue is empty");
		if (size == 0)
		{
			cout << "Deque is empty. Invalid frontLink." << endl;
			abort();
		}
		else
		{
			return front->next;
		}
	}


	Link<E>* getFront() const
	{ // Get the front pointer
		
		return front;
	}

	Link<E>* getRear() const
	{ // Get rear pointer
		return rear;
	}
	virtual int length() const
	{
		return size;
	}

	int halver() //Finds the half-way point for THIS deck, no parameters
	{
		int half = 0;
		if (length() == 0) return half;           // Empty deck
		if (length() % 2 == 0) // If the length is an even number, set the half-way point by just dividing by two
		{
			half = length() / 2;
		}
		if (length() % 2 == 1) // If the length is an odd number, set the half-way point by integer dividing by two and then adding one to point to the 'center'
		{
			half = (length() / 2) + 1;
		}
		return half;
	}
	int halver(LDeck<E>* checkDeck) //Finds the half-way point for a deck
	{
		int half = 0;
		if (checkDeck->length() == 0) return half;           // Empty deck
		if (checkDeck->length() % 2 == 0) // If the length is an even number, set the half-way point by just dividing by two
		{
			half = checkDeck->length() / 2;
		}
		if (checkDeck->length() % 2 == 1) // If the length is an odd number, set the half-way point by integer dividing by two and then adding one to point to the 'center'
		{
			half = (checkDeck->length() / 2) + 1;
		}
		return half;
	}

	void readOut() //Prints out and empties the deck. 
	{
		while (size > 0)
		{
			if (size == 1) //Last card
			{
				cout << dequeue() << ". " << endl << endl;
			}
			else
			{
				cout << dequeue() << ", ";
			}
		}
		return;
	}

};

#endif