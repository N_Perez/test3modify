/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Game Project
deque links/cards
*/
#pragma once
#ifndef LINK_H
#define LINK_H
#define NULL 0

template < typename E> class Link {
public:
  E element;      // Value for this node
  Link *next;        // Pointer to next node in list
  Link *prev;        // Pointer to last node in list
  // Constructors
  Link(const E& elemval, Link* nextval =NULL, Link* prevval =NULL) //Constructor, with an initial value supplied
    {
		element = elemval;  
		next = nextval; 
		prev = prevval;
	}
  Link(Link* nextval =NULL, Link* prevval =NULL) //Constructor, without an initial value
	{
	  next = nextval; 
	  prev = prevval;
	}
};
#endif