#pragma once


#include "Node.h"
#include <iostream>

using namespace std;
class Queue
{
	Node<int>* front;       // Pointer to front queue node
	Node<int>* rear;        // Pointer to rear queue node
	int size;                // Number of elements in queue

public:
	Queue()
	{
		front = rear = new Node<int>(); // Creates a new node, sets front and rear pointers to point at it
		size = 0;
	}
	~Queue()
	{

	}

	void enqueue(const int& it) // Put element on rear
	{
		rear->next = new Node<int>(it, nullptr); //Sets the rear 'next' pointer to point to a new link
		rear = rear->next; //sets the 'rear' pointer to point to the new link
		size++;
	}
	int dequeue() // Remove element from front
	{
		if (size == 0)
		{
			cout << "Queue is empty. Invalid dequeue" << endl;
			abort();
		}

		else
		{
			int it = front->next->element;  // Store dequeued value
			Node<int>* ltemp = front->next; // Hold dequeued link
			front->next = ltemp->next;       // Advance front
			if (rear == ltemp) // Dequeue last element
			{
				rear = front; 
				front->next = nullptr;
			}
			delete ltemp;                    // Delete link
			size--;
			return it;                       // Return element value
		}
	}

	void printAll() //Prints and empties the queue
	{
		cout << "Printing Queue: " << endl;
		while (size > 0)
		{
			if (size == 1) //Last card
			{
				cout << dequeue() << ". " << endl << endl;
			}
			else
			{
				cout << dequeue() << ", ";
			}
		}
	}

	virtual int length() const
	{
		return size;
	}
};

